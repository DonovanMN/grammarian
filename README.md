# Setup

Create venv, activate it, and install Python dependencies

```sh
$ python3 -m venv .venv
$ . .venv/bin/activate
$ pip install -r requirements.txt
```

Some scripts also depend on the existence of the `/usr/share/dict/words` system dictionary. Install the requisite system package to provide this file if it is not included in the distro by default.

For example, on Ubuntu, install the American English word list package:

```sh
$ sudo apt install wamerican
```

# Usage

Run grammarian script, specifying each word of the spell name in lowercase separated by spaces:

```sh
$ ./scripts/grammarian.py minor illusion
```

## Customize alternative word options for a set of spells

Generate an index file of unique words, passing a glob of files containing spell names, one per line:

```sh
$ ./scripts/unique_words.py spells/cleric/* > output/cleric_index.txt
```

Then use the index to generate files with all the alternatives for each word:

```sh
$ ./scripts/save_similar_words.sh output/cleric_index.txt words
```

The files created in the words directory can be edited to add or remove lines as desired.
Running the grammarian.py script will now only use the alternative words in those files.
