#!/bin/bash

index=$1
outdir=$2

cat $index | while read line; do
    ./scripts/find_similar.py ${line} > ${outdir}/${line}.txt
done

