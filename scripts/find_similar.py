#!/usr/bin/env python3
import sys
from levenshtein_finder import LevenshteinFinder

finder = LevenshteinFinder()
finder.indexing('/usr/share/dict/words')


def find_similar(query):
    return set([result['data'].lower() for result in finder.search(query, max_distance=1) if result['distance'] == 1])


def main():
    similar_words = find_similar(sys.argv[1])
    for word in similar_words:
        print(word)


if __name__ == '__main__':
    main()
