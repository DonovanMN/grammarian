#!/usr/bin/env python3
import sys
import re


def unique_words_in_files(filenames):
    delimiters = re.compile(' |/')
    words = set()
    for filename in filenames:
        with open(filename) as file:
            lines = file.read().splitlines()
            new_words = [word for line in lines for word in delimiters.split(line) if word]
            words.update(new_words)
    return words


def main():
    args = sys.argv[1:]
    words = unique_words_in_files(args)
    for word in words:
        print(word)

    
if __name__ == "__main__":
    main()
