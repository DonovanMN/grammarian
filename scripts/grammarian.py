#!/usr/bin/env python3
import sys

from find_similar import find_similar

def grammarian(spell):
    #loop through words
    results = []
    for idx, word in enumerate(spell):
        alternates = None
        try:
            with open(f"words/{word}.txt") as file:
                alternates = file.read().splitlines()
        except:
            alternates = find_similar(word)
        for alternate in alternates:
            new_spell = spell.copy()
            new_spell[idx] = alternate
            results.append(' '.join(new_spell))
    return results

def main():
    args = sys.argv[1:]
    spells = grammarian(args)
    for spell in spells:
        print(spell)
    
if __name__ == "__main__":
    main()